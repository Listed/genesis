//
//  GSRecordViewController.swift
//  Genesis
//
//  Created by admin on 6/8/19.
//  Copyright © 2019 Andrew Sasin. All rights reserved.
//

import UIKit
import AVFoundation
import RxSwift

class GSRecordViewController: UIViewController {

    private var dispoceBag = DisposeBag()
    let rxNewPath = PublishSubject<String>()
    
    lazy var recordButton : UIButton = {
        let button = UIButton()
        button.setTitle("Record", for: .normal)
        button.backgroundColor = UIColor.red
        button.layer.cornerRadius = 50
        button.addTarget(self, action: #selector(actionRecord), for: .touchUpInside)
        return button
    }()
    
    lazy var recordTime: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.text = "00:00"
        return label
    }()
    
    var audioTimer: Timer?
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder?
    
    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        recordingSession = AVAudioSession.sharedInstance()
        
        checkPermission { (isAccess) in
            DispatchQueue.main.async {
                if isAccess{
                    
                    do {
                        try self.recordingSession.setCategory(.playAndRecord, mode: .default)
                        try self.recordingSession.setActive(true)
                    } catch{
                        print("Have trouble with record seession")
                    }
                    
                    self.view.addSubview(self.recordButton)
                    self.view.addSubview(self.recordTime)
                    
                    self.recordButton.snp.makeConstraints { (make) -> Void in
                        make.width.equalTo(100)
                        make.height.equalTo(100)
                        make.centerX.equalToSuperview()
                        make.centerY.equalToSuperview()
                    }
                    
                    self.recordTime.snp.makeConstraints({ (make) in
                        make.bottom.equalTo(self.recordButton).offset(40)
                        make.centerX.equalTo(self.recordButton)
                    })
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        audioTimer?.invalidate()
        audioRecorder?.stop()
        audioRecorder = nil
    }
    
    deinit {
        print("Deinitalize \(String(describing: GSRecordViewController.self))")
    }
    
    //MARK: Methods
    private func checkPermission(completionHandler: @escaping (Bool) -> Void){
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSessionRecordPermission.granted:
            print("Permission granted")
            completionHandler(true)
        case AVAudioSessionRecordPermission.denied:
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Go to Settings", message: "Open Settings to permission audio record", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    if let appSettings = URL(string: UIApplication.openSettingsURLString + Bundle.main.bundleIdentifier!) {
                        if UIApplication.shared.canOpenURL(appSettings) {
                            UIApplication.shared.open(appSettings)
                        }
                    }
                }))
                
                self.present(alert, animated: true)
            }
            completionHandler(false)
        case AVAudioSessionRecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                if granted {
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
            })
        default:
            break
        }
    }
    
    //MARK -------AUDIO-------
    func startRecording() {
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 44100,
            AVNumberOfChannelsKey: 2,
            AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue
        ]
        
        do {
            audioTimer?.invalidate()
            
            audioTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(runTimedAudio), userInfo: nil, repeats: true)
            
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder?.delegate = self
            audioRecorder?.record()
            
            recordButton.setTitle("Stop", for: .normal)
            
        } catch {
            finishRecording(success: true)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func finishRecording(success: Bool) {
        audioTimer?.invalidate()
        audioRecorder?.stop()
        audioRecorder = nil
        
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")
        let asset = AVAsset(url: audioFilename)
        let length = Double((Float(asset.duration.value) / Float(asset.duration.timescale)) / 3)
        print("auido length: \(length) seconds")
        //2019-май-20/15:47:23 part 1
        //splitAudio(sourceURL: audioFilename, startTime: 0.0, endTime: length)
        
        let formatterToRequest = DateFormatter()
        formatterToRequest.dateFormat = "yyyy-MMM-dd  HH:mm:ss"
        formatterToRequest.locale =  Locale(identifier:"ru")
        
        let timeDeparture = formatterToRequest.string(from:Date())
        
        let partsURL = getDocumentsDirectory().appendingPathComponent("output")
        
        splitAudio(sourceURL: audioFilename, startTime: 0, endTime: length, outputURL: (partsURL.appendingPathComponent("\(timeDeparture) part 1.mp4")))
        splitAudio(sourceURL: audioFilename, startTime: length, endTime: length*2, outputURL: (partsURL.appendingPathComponent("\(timeDeparture) part 2.mp4")))
        splitAudio(sourceURL: audioFilename, startTime: length*2, endTime:  Double((Float(asset.duration.value) / Float(asset.duration.timescale))), outputURL: (partsURL.appendingPathComponent("\(timeDeparture) part 3.mp4")))
        
        if success {
            recordButton.setTitle("Record", for: .normal)
        } else {
            recordButton.setTitle("Stop", for: .normal)
        }
    }
    
    func splitAudio(sourceURL: URL, startTime: Double, endTime: Double, outputURL:URL ,completion: ((_ completionOutputUrl: URL) -> Void)? = nil)
    {
        let fileManager = FileManager.default
        let documentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        let asset = AVAsset(url: sourceURL)
        
        let createPath = documentDirectory.appendingPathComponent("output")
        do {
            try fileManager.createDirectory(at: createPath, withIntermediateDirectories: true, attributes: nil)
        }catch let error {
            print(error)
        }
        
        //Remove existing file
        //try? fileManager.removeItem(at: outputURL)
        
        guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetAppleM4A) else { return }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = .m4a
        
        let timeRange = CMTimeRange(start: CMTime(seconds: startTime, preferredTimescale: 100),
                                    end: CMTime(seconds: endTime, preferredTimescale: 100))
        
        exportSession.timeRange = timeRange
        exportSession.exportAsynchronously {
            switch exportSession.status {
            case .completed:
                self.rxNewPath.onNext(outputURL.absoluteString)
                print("exported at \(outputURL)")
                completion?(outputURL)
            case .failed:
                print("failed \(exportSession.error.debugDescription)")
            case .cancelled:
                print("cancelled \(exportSession.error.debugDescription)")
            default: break
            }
        }
    }
    
    //MARK: Actions
    @objc func  runTimedAudio (){
        guard let audio = audioRecorder else {
            return
        }
        
        if audio.isRecording{

            recordTime.text = "\(String(describing: audioRecorder!.currentTime.stringFromTimeInterval()))"
        }
        
    }
    
    @objc func actionRecord(sender: UIButton!) {
        if audioRecorder != nil{
            if !audioRecorder!.isRecording {
                
                startRecording()
            }else{
                audioTimer?.invalidate()
                audioRecorder?.stop()
                audioRecorder = nil
            }
        }else{
            startRecording()
        }
    }
}


extension GSRecordViewController:AVAudioRecorderDelegate{
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }else{
            finishRecording(success: true)
        }
    }
}
