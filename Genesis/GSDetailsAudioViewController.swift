//
//  GSDetailsAudioViewController.swift
//  Genesis
//
//  Created by admin on 6/9/19.
//  Copyright © 2019 Andrew Sasin. All rights reserved.
//

import UIKit
import AVFoundation
import RxSwift

class GSDetailsAudioViewController: UIViewController {

    let rxNewValue = PublishSubject<String>()
    var urlPath: String = ""
    
    lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 5
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    lazy var mainScrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        return scroll
    }()
    
    lazy var viewLabe: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var labelName: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    
    lazy var labelTime: UILabel = {
        let label = UILabel()
        return label
    }()
    
    lazy var viewButton: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var waveView : GSWaveWormView = {
        let view = GSWaveWormView()
        view.backgroundColor = UIColor.white
        return view
    }()

    lazy var playButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Play", for: .normal)
        button.backgroundColor = UIColor.red
        button.layer.cornerRadius = 50
        button.addTarget(self, action: #selector(actionPlay), for: .touchUpInside)
        return button
    }()
    
    var objPlayer: AVAudioPlayer?
    var audioTimer: Timer?
    
    lazy var serial : DispatchQueue = {
        let seial = DispatchQueue(label: "Some Label", qos: .userInitiated)
        return seial
    }()
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        createUI()
        
//        //DispatchQueue.global(qos: .userInitiated).async {[weak self] in
//            guard let self = self else {
//                return
//            }
        
            let file = try! AVAudioFile(forReading: URL(string: self.urlPath)!)
            let format = AVAudioFormat(commonFormat: .pcmFormatFloat32, sampleRate: file.fileFormat.sampleRate, channels: file.fileFormat.channelCount, interleaved: false)
            print(file.fileFormat.channelCount)
            let buf = AVAudioPCMBuffer(pcmFormat: format!, frameCapacity: UInt32(file.length))
            try! file.read(into: buf!)
            
            let result = Array(UnsafeBufferPointer(start: buf?.floatChannelData?[0], count:Int(buf!.frameLength))).enumerated().filter({ index, _ in
                index % 4 == 0
            }).map { $0.1 }
            
            
            readFile.arrayFloatValues = result
            
            //DispatchQueue.main.async {
              //  self.waveView.setNeedsDisplay()
           // }
        
        waveView.isHidden = true
       // }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         waveView.isHidden = false
        
        waveView.snp.updateConstraints { (make) in   make.width.equalTo(Double(readFile.points.count) * 2.5)}
        waveView.setNeedsDisplay()
        
        //waveView.snp.updateConstraints { (make) in   make.width.equalTo(readFile.points.count * 2)}
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopAudio()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        //TODO: NEED: change vawe vizualizations
        print("NEED: change vawe vizualizations")
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
        } else {
            print("Portrait")
        }
    }
    
    //MARK: Actions
    private func createUI(){
        self.view.addSubview(mainScrollView)
        
        mainScrollView.addSubview(stackView)
        stackView.addArrangedSubview(viewLabe)
        stackView.addArrangedSubview(scrollView)
        stackView.addArrangedSubview(viewButton)
        scrollView.addSubview(waveView)
        viewButton.addSubview(playButton)
        viewLabe.addSubview(labelName)
        viewLabe.addSubview(labelTime)
        
        scrollView.isScrollEnabled = true
        
        viewLabe.snp.makeConstraints { (make) -> Void in
            make.trailing.equalToSuperview()
            make.leading.equalToSuperview()
            make.top.equalToSuperview()
            make.height.greaterThanOrEqualTo(10)
        }
        
        labelName.snp.makeConstraints { (make) -> Void in
            //make.edges.equalToSuperview()
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(17)
            //make.bottom.equalToSuperview()
            make.right.equalToSuperview()
        }
    
        labelTime.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(labelName.snp_bottomMargin).offset(10)
            make.leading.equalToSuperview().offset(17)
            make.trailing.equalToSuperview()
            make.bottomMargin.equalToSuperview()
        }
        
        mainScrollView.snp.makeConstraints { (make) -> Void in
            make.edges.equalToSuperview()
        }
        
        stackView.snp.makeConstraints { (make) -> Void in
            make.edges.equalToSuperview()
            make.width.equalTo(self.view)
        }
        
        viewButton.snp.makeConstraints { (make) -> Void in
            //make.edges.equalToSuperview()
            make.height.equalTo(100)
        }
        
        playButton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(100)
            make.width.equalTo(100)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        scrollView.snp.makeConstraints { (make) -> Void in
            // make.edges.equalToSuperview()
            make.height.equalTo(300)
        }
    
        waveView.snp.makeConstraints { (make) -> Void in
            //make.width.equalTo(300)
            make.leadingMargin.equalToSuperview()
            make.trailing.equalToSuperview()
            make.top.equalToSuperview()
            make.height.equalToSuperview()
            make.width.equalTo(300)
        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(removeTapped))
        
        let url = URL(string: urlPath)
        labelName.text = url?.lastPathComponent
        labelTime.text = url!.createTimeFormUrl()
    }
    
    func playAudioFile(urlString:String?) {
        guard let url = URL(string: urlString ?? "") else {
            print("url not found")
            return
        }
    
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try AVAudioSession.sharedInstance().setActive(true)
            
           
            
            if objPlayer != nil{
                stopAudio()
                waveView.progress = 0
                waveView.needUpdate = true
                waveView.setNeedsDisplay()
                playButton.setTitle("Play", for: .normal)
            }else{
                
                objPlayer = try AVAudioPlayer.init(contentsOf: url)
                
                guard let aPlayer = objPlayer else { return }
                
                audioTimer?.invalidate()
                
                audioTimer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(runTimedAudio), userInfo: nil, repeats: true)
                
                if audioTimer != nil{
                    RunLoop.current.add(audioTimer!, forMode: RunLoop.Mode.common)
                }
                
                playButton.setTitle("Stop", for: .normal)
                
                aPlayer.delegate = self
                aPlayer.prepareToPlay()
                aPlayer.play()
            }
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    @objc func actionPlay(sender: UIButton!){
        playAudioFile(urlString: urlPath)
    }
    
    
    @objc func  runTimedAudio (){
        guard let audio = objPlayer else {
            return
        }
//        serial.async {[weak self] in
//            guard let self = self else {
//                return
//            }
        
            if audio.isPlaying{
                
                
                let progress = Float(audio.currentTime / audio.duration)
                //let a = Float(readFile.arrayFloatValues.count * 4) * progress
                
                //DispatchQueue.main.async {
                self.waveView.firstUpdate = false
                    self.waveView.progress = progress
                    self.waveView.needUpdate = true
                    self.waveView.setNeedsDisplay()
            
                //}
                
//                let url = URL(string: self.urlPath)
//                let file = try! AVAudioFile(forReading: url!)
//                let format = AVAudioFormat(commonFormat: .pcmFormatFloat32, sampleRate: file.fileFormat.sampleRate, channels: file.fileFormat.channelCount, interleaved: false)
//                print(file.fileFormat.channelCount)
//                let buf = AVAudioPCMBuffer(pcmFormat: format!, frameCapacity: UInt32(file.length))
//                try! file.read(into: buf!)
//                print("\(file.length) \(a)  \(progress)")
//
//                let result = Array(UnsafeBufferPointer(start: buf?.floatChannelData?[0], count:Int(a))).enumerated().filter({ index, _ in
//                    index % 4 == 0
//                }).map { $0.1 }
//
//                readFile.arrayPalyingFloatValues = result
//
//                DispatchQueue.main.async {
//                    self.waveView.needUpdate = true
//                    self.waveView.setNeedsDisplay()
//                }
            }else{
                waveView.progress = 0
                //DispatchQueue.main.async {
                    self.waveView.needUpdate = true
                    self.waveView.setNeedsDisplay()
                //}
            }
        //}
    }
    
    @objc func removeTapped(){
        stopAudio()
        rxNewValue.onNext(urlPath)
        navigationController?.popViewController(animated: true)
    }
    
    private func stopAudio(){
        audioTimer?.invalidate()
        objPlayer?.stop()
        objPlayer = nil
    }
}

extension GSDetailsAudioViewController:AVAudioPlayerDelegate{
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        stopAudio()
        playButton.setTitle("Play", for: .normal)
        waveView.progress = 0
        waveView.needUpdate = true
        waveView.setNeedsDisplay()
    }
}
