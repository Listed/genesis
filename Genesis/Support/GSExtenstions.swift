//
//  GSExtenstions.swift
//  Genesis
//
//  Created by admin on 6/8/19.
//  Copyright © 2019 Andrew Sasin. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

extension TimeInterval{
    
    func stringFromTimeInterval() -> String {
        
        let time = NSInteger(self)
        
        let ms = Int((self.truncatingRemainder(dividingBy: 1)) * 1000)
        let seconds = time % 60
        let minutes = (time / 60) % 60
        
        return String(format: "%0.2d:%0.2d:%0.2d",minutes,seconds,ms)
        
    }
}

extension URL{
     func createTimeFormUrl () -> String{
        let asset = AVAsset(url: self)
        let duration = asset.duration
        let durationTime = CMTimeGetSeconds(duration)
        
        let ms = Int(durationTime.truncatingRemainder(dividingBy: 1) * 1000)
        let seconds = Int(durationTime) % 60
        let minutes = (Int(durationTime) / 60) % 60
        
        return String(format: "%0.2d:%0.2d:%.3d",minutes,seconds,ms)
    }
}


extension FileManager {
    func urls(skipsHiddenFiles: Bool = true ) -> [String] {
        
        var allPath :[String] = []
        
        let fileManager = FileManager.default
        let documentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let createPath = documentDirectory.appendingPathComponent("output")
        let fileURLs = try? contentsOfDirectory(at: createPath, includingPropertiesForKeys: nil, options: skipsHiddenFiles ? .skipsHiddenFiles : [] )
        
        if fileURLs != nil{
            let sortedUsers = fileURLs!.sorted {
                $0.absoluteString > $1.absoluteString
            }
            
            for url in sortedUsers{
                allPath.append(url.absoluteString)
            }
        }
        return allPath
    }
}

