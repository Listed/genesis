//
//  ViewController.swift
//  Genesis
//
//  Created by admin on 6/7/19.
//  Copyright © 2019 Andrew Sasin. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import AVFoundation

class GSRecordTableViewController: UIViewController {
    
    let tableView = UITableView()
    var objPlayer: AVAudioPlayer?
    var indexPlaying:IndexPath?
    var audioTimer: Timer?
    
    let cellIdentifier = "AudioCell"
    
    //MARK: Life cycle
    let disposeBag = DisposeBag()
    var data: BehaviorRelay<[String]> = BehaviorRelay(value: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //UI
        createUI()
        
        //RX Table
        
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let self = self else {
                return
            }
            
            let allPath = FileManager.default.urls(skipsHiddenFiles: true)
            
            self.data.accept(self.data.value + allPath)
        }
        
        data.bind(to: tableView.rx.items(cellIdentifier: cellIdentifier, cellType: GSAudioTableViewCell.self)) { index, model, cell in
            
            if index == self.indexPlaying?.row{
                cell.playAudioButton.backgroundColor = UIColor.green
                cell.playAudioButton.setTitle("Stop", for:  .normal)
            }else{
                cell.playAudioButton.backgroundColor = UIColor.red
                cell.playAudioButton.setTitle("Play", for:  .normal)
                cell.progressView.progress = 0.0
            }
            cell.createCell(withPath: model)
            
            cell.playAudioButton.rx.tap.asDriver()
                .drive(onNext: { [weak self] in
                    guard let indexPath = self?.tableView.indexPath(for: cell) else {
                        return
                    }
                    
                    if self?.indexPlaying != nil{
                        if let cellStop = self?.tableView.cellForRow(at: self!.indexPlaying!) as? GSAudioTableViewCell{
                            cellStop.playAudioButton.backgroundColor = UIColor.red
                            cellStop.playAudioButton.setTitle("Play", for:  .normal)
                            cellStop.progressView.progress = 0.0
                        }
                    }
                    
                    self?.audioTimer?.invalidate()
                    
                    if self?.objPlayer != nil{
                        self?.objPlayer!.stop()
                        self?.objPlayer = nil
                    }
                    
                    if self?.indexPlaying?.row == indexPath.row{
                        cell.playAudioButton.backgroundColor = UIColor.red
                        cell.playAudioButton.setTitle("Play", for:  .normal)
                        cell.progressView.progress = 0.0
                        self?.indexPlaying = nil
                    }else{
                        
                        self?.audioTimer = Timer.scheduledTimer(timeInterval: 0.001, target: self as Any, selector: #selector(self?.runTimedAudio), userInfo: nil, repeats: true)
                        
                        if self?.audioTimer != nil{
                            RunLoop.current.add(self!.audioTimer!, forMode: RunLoop.Mode.common)
                        }
                        
                        cell.playAudioButton.backgroundColor = UIColor.green
                        cell.playAudioButton.setTitle("Stop", for:  .normal)
                        
                        self?.indexPlaying = indexPath
                        self?.playAudioFile(urlString: self?.data.value[indexPath.row])
                    }
                }).disposed(by: cell.disposeBag)
            
            }.disposed(by: disposeBag)
        
        //RX Selected Element
        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                if self?.tableView.cellForRow(at: indexPath) as? GSAudioTableViewCell != nil{
                    let detailVS = GSDetailsAudioViewController()
                    detailVS.urlPath = (self?.data.value[indexPath.row])!
                    detailVS.rxNewValue.subscribe(onNext: { [weak self] string in
                        guard let strongSelf =  self else { return }
                        var array = strongSelf.data.value
                        let indexOf = array.firstIndex(of:string)
                        
                        if indexOf != nil{
                            do {
                                let fileManager = FileManager.default
                                let urlPath = URL(string: array[indexOf!])
                                try fileManager.removeItem(at: urlPath!)
                            }catch{
                                print("Could not clear temp folder: \(error)")
                            }
                            
                            array.remove(at: indexOf!)
                            strongSelf.data.accept(array)
                        }
                    }).disposed(by: self!.disposeBag)
                    detailVS.view.backgroundColor = UIColor.white
                    self?.navigationController?.pushViewController(detailVS, animated: true)
                }
            }).disposed(by: disposeBag)
        
        //RX Delete Element need add animations when delete
        
        tableView.rx.itemDeleted
            .subscribe(onNext: { [weak self] indexPath in
                var array = self?.data.value
                
                do {
                    let fileManager = FileManager.default
                    let urlPath = URL(string: array![indexPath.row])
                    try fileManager.removeItem(at: urlPath!)
                }catch{
                    print("Could not clear temp folder: \(error)")
                }
                
                array?.remove(at: indexPath.row)
                self?.data.accept(array!)
            })
            .disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let index = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: index, animated: true)
            self.tableView.reloadRows(at: [index], with: .automatic)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        audioTimer?.invalidate()
        if objPlayer != nil{
            if objPlayer!.isPlaying{
                if indexPlaying != nil{
                    if let cellStop = tableView.cellForRow(at: indexPlaying!) as? GSAudioTableViewCell{
                        cellStop.playAudioButton.backgroundColor = UIColor.red
                        cellStop.playAudioButton.setTitle("Play", for:  .normal)
                        cellStop.progressView.progress = 0.0
                    }
                    indexPlaying = nil
                }
            }
            objPlayer!.stop()
            objPlayer = nil
        }
    }
    
    deinit {
        print("Deinitalize")
    }
    
    //MARK: Actions
    private func createUI(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        
        tableView.estimatedRowHeight = 300
        tableView.rowHeight = UITableView.automaticDimension
        
        self.view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.view)
            make.bottom.equalTo(self.view)
            make.right.equalTo(self.view)
            make.left.equalTo(self.view)
        }
        
        tableView.register(GSAudioTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        
    }
    
    private func playAudioFile(urlString:String?) {
        
        guard let url = URL(string: urlString ?? "") else {
            print("url not found")
            return
        }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            objPlayer = try AVAudioPlayer.init(contentsOf: url)
            
            guard let aPlayer = objPlayer else { return }
            
            
            objPlayer?.delegate = self
            aPlayer.prepareToPlay()
            aPlayer.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    @objc func  runTimedAudio (){
        guard let audio = objPlayer else {
            return
        }
        
        if audio.isPlaying{
            if indexPlaying != nil{
                if let cellStop = tableView.cellForRow(at: indexPlaying!) as? GSAudioTableViewCell{
                    cellStop.progressView.progress = Float(audio.currentTime / audio.duration)
                    
                    print("\(audio.currentTime) \(audio.duration)")
                }
            }
        }
        
    }
    
    @objc func addTapped(){
        let recordVS = GSRecordViewController()
        recordVS.rxNewPath.subscribe(onNext: { [weak self] string in
            guard let strongSelf =  self else { return }
            strongSelf.data.accept([string] + strongSelf.data.value)
        }).disposed(by: disposeBag)
        recordVS.view.backgroundColor = UIColor.white
        navigationController?.pushViewController(recordVS, animated: true)
    }
}

extension GSRecordTableViewController:AVAudioPlayerDelegate{
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if flag {
            
            audioTimer?.invalidate()
            
            if indexPlaying != nil{
                if let cellStop = tableView.cellForRow(at: indexPlaying!) as? GSAudioTableViewCell{
                    cellStop.playAudioButton.backgroundColor = UIColor.red
                    cellStop.playAudioButton.setTitle("Play", for:  .normal)
                    cellStop.progressView.progress = 0.0
                }else{
                    indexPlaying = nil
                }
            }
        }
    }
}
