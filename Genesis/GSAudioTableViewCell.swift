//
//  GSAudioTableViewCell.swift
//  Genesis
//
//  Created by admin on 6/8/19.
//  Copyright © 2019 Andrew Sasin. All rights reserved.
//

import UIKit
import RxSwift
import AVFoundation

class GSAudioTableViewCell: UITableViewCell {

    var disposeBag = DisposeBag()
    let subject = PublishSubject<Void>()
    
    override func prepareForReuse() {
        disposeBag = DisposeBag()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    let nameAudio : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let progressView: UIProgressView = {
        let progress = UIProgressView()
        progress.progress = 0.0
        
        progress.layer.cornerRadius = 10
        progress.clipsToBounds = true
        progress.layer.sublayers![1].cornerRadius = 10
        progress.subviews[1].clipsToBounds = true

        return progress
    }()
    
    let timeAudio : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let playAudioButton : UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.red
        button.layer.cornerRadius = 50
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(nameAudio)
        addSubview(timeAudio)
        addSubview(playAudioButton)
        addSubview(progressView)
        
        timeAudio.text = "asdgasdgsdagasdgsadggasd"
        
        playAudioButton.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self).offset(17)
            make.height.equalTo(100)
            make.width.equalTo(100)
            make.leftMargin.equalTo(self).offset(17)
        }
        
        progressView.snp.makeConstraints { (make) -> Void in
            make.topMargin.equalTo(playAudioButton.snp_bottomMargin).offset(30)
            make.rightMargin.equalTo(self).offset(-17)
            make.leftMargin.equalTo(self).offset(17)
            make.bottom.equalTo(self).offset(-17)
        }
        
        nameAudio.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self).offset(17)
            make.rightMargin.equalTo(self).offset(-17)
            make.left.equalTo(playAudioButton.snp_rightMargin).offset(17)
        }
        
        timeAudio.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(nameAudio.snp_bottomMargin).offset(17)
            make.rightMargin.equalTo(self).offset(-17)
            make.leading.equalTo(nameAudio)
            make.bottom.lessThanOrEqualTo(playAudioButton).offset(-10)
        }
        

        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func createCell(withPath: String){
        guard let url = URL(string: withPath) else { return }
        
        nameAudio.text = url.lastPathComponent.replacingOccurrences(of: "  ", with: "/")
    
        timeAudio.text = url.createTimeFormUrl()
    }

}
